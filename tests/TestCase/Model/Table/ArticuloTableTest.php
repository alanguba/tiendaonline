<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ArticuloTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ArticuloTable Test Case
 */
class ArticuloTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ArticuloTable
     */
    protected $Articulo;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Articulo',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Articulo') ? [] : ['className' => ArticuloTable::class];
        $this->Articulo = $this->getTableLocator()->get('Articulo', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Articulo);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
