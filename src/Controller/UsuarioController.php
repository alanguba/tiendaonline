<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Usuario Controller
 *
 * @property \App\Model\Table\UsuarioTable $Usuario
 * @method \App\Model\Entity\Usuario[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsuarioController extends AppController
{
    
    /**
     * Método login
     * Se encarga de la funcionalidad de login.
     *
     * @param string|null $id Usuario id.
     */
    public function login()
    {
        if ($this->request->is('post')) {
            //Se obtiene la información del formulario
            $usuarios = $this->getTableLocator()->get('Usuario');
            $usuario = $usuarios->find()
            ->where(['email' => $this->request->getData('email')])
            ->first();
            //Se pregunta si el email existe
            if ($usuario==[]) {
                $this->Flash->error(__('Usario incorrecto.'));
            } else {
                //Se pregunta si la contraseña es correcta
                if ($usuario->password==$this->request->getData('password')) {
                    $this->Flash->success(__('Inicio de sesión exitoso.'));
                    //Se redirige a la página principal
                    return $this->redirect(['controller' => 'Venta','action' => 'index', $usuario->idUsuario]);
                } else {
                    $this->Flash->error(__('Contraseña incorrecta.'));
                }
            }
        }
        $this->set('login', true);
    }
}
