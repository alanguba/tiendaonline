<?php

namespace App\Controller;

/**
 * Static content controller
 *
 * This controller will render views from templates/Pages/
 *
 * @link https://book.cakephp.org/4/en/controllers/pages-controller.html
 */
class VentaController extends AppController
{
    /**
     * Método index
     * Es la página principal de la tienda en línea, donde se muestran todos los artículos disponibles.
     *
     * @param string|null $id Usuario id.
     */
    public function index($id = null)
    {
        //Se pregunta si tenemos el id del usuario
        if($id!=null) {
            //Se obtiene la infomación del usuario
            $usuariosTable = $this->getTableLocator()->get('usuario');
            $usuario = $usuariosTable->get($id);
            //Se obtiene la información de todos los artículos
            $articulosTabla = $this->getTableLocator()->get('articulo');
            $articulos = $articulosTabla->find('all');
            //Se manda la información a la vista
            $this->set(compact('articulos'));
            $this->set(compact('usuario'));
        //Se redirige al login, en dado caso de no contar con el id
        } else {
            $this->redirect(['controller'=>'Usuario','action'=>'login']);
        }
    }

    public function comprar($idArticulo = null, $idUsuario = null)
    {
        //Se verifica que tanto el usuario haya inciiado sesión y que el artículo haya sido seleccionado.
        if($idArticulo==null || $idUsuario==null) {
            return $this->redirect(['controller'=>'venta','action'=>'index', $idUsuario]);
        }
        //Se obtiene el objeto que apunta hacia la tabla 'Articulo' de la Base de datos.
        $articlesTable = $this->getTableLocator()->get('articulo');
        //Se obtiene el objeto que apunta hacia la tabla 'Usuario' de la Base de datos.
        $usuariosTable = $this->getTableLocator()->get('usuario');
        //Se obtiene el artículo y el usuario segñun los IDs pasados a través de la URL.
        $article = $articlesTable->get($idArticulo);
        $usuario = $usuariosTable->get($idUsuario);
        //Si el método del envío del formulario es put
        if ($this->request->is('put')) {
            // Se maneja la concurrencia preguntando si el objeto está comprandose por alguien mas
            $article = $articlesTable->get($idArticulo);
            // Se pregunta si el artículo ya tiene dueño.
            if(!$article->idUsuario) {
                // Se pregunta si dinero del comprador es suficiente.
                    $dataSource = $this->getTableLocator()->get('articulo');
                    $dataSource2 = $this->getTableLocator()->get('usuario');
                    // Se asocia el artículo al usuario y se almacena en la BD.
                    $dataSource->getConnection()->begin();
                    //Se implementa el commit
                    if($this->request->getData('credito') >= 0) {
                        $article->idUsuario = $usuario->idUsuario;
                        $dataSource->saveOrFail($article, ['atomic' => false]);
                        $dataSource->getConnection()->commit();
                        $usuario = $usuariosTable->patchEntity($usuario, $this->request->getData());
                        $dataSource2->getConnection()->begin();
                        $dataSource2->saveOrFail($usuario, ['atomic' => false]);
                        $dataSource2->getConnection()->commit();

                        // Se envian los mensajes de éxito y se redirecciona a la página principal.
                        $this->Flash->success(__('Has comprado el artículo con éxito'));
                        return $this->redirect(['controller'=>'venta','action'=>'index', $idUsuario]);
                    }
                    else {
                        $this->Flash->error(__('No tienes puntos suficientes para realizar la compra.'));
                        $dataSource->getConnection()->rollback();
                        $dataSource2->getConnection()->rollback();
                    }
            } else {
                $this->Flash->error(__('Otro usuario acaba de comprar el artículo.'));
            }
            
        }
        // Se envian los datos del usuario y del artículo a la vista de compra
        $this->set(compact('usuario'));
        $this->set('articulo', $article);
    }

    /**
     * Método editar
     * Se encarga de guardar la información modificada del artículo, además de comprobar si
     * el artpiculo está siendo editado.
     *
     * @param string|null $id Usuario id.
     */
    public function editar ($idArticulo = null, $idUsuario = null)
    {
        //Se pregunta si tenemos el id del artículo y del usuario
        if($idArticulo==null || $idUsuario==null) {
            $this->redirect(['controller'=>'Usuario','action'=>'login']);
        }
        
        //Se obtiene la información del artículo y del usuario
        $articlesTable = $this->getTableLocator()->get('articulo');
        $usuariosTable = $this->getTableLocator()->get('usuario');

        $articulo = $articlesTable->get($idArticulo);
        $usuario = $usuariosTable->get($idUsuario);

        //Se pregunta si se está guardando la información del artículo
        if ($this->request->is(['put'])) {
            //Se obtiene la tabla para hacer la transacción manualmente
            $dataSource = $this->getTableLocator()->get('articulo');
            //Se modifica el estado de bloqueo
            $articulo->bloqueo = false;

            //Se guarda la información modificada haciendo un commit
            try {
                $dataSource->getConnection()->begin();
                $dataSource->saveOrFail($articulo, ['atomic' => false]);
                $dataSource->getConnection()->commit();
                $this->Flash->success(__('Se editó el archivo correctamente.'));
            //Se hace un rollback en dado caso de no haberse guardado la información de forma correcta
            } catch(\Cake\ORM\Exception\PersistenceFailedException $e) {
                $dataSource->getConnection()->rollback();
                $this->Flash->error(__('Error al momento de guardar la información.'));
            }

            //Se redirige al página principal
            return $this->redirect(['controller'=>'venta','action'=>'index', $idUsuario]);
        }

        //Se pregunta si el artículo está en estado de bloqueo para mandar un mensaje
        //y no permitir que alguien más pueda editar el artículo
        if($articulo->bloqueo) {
            $this->Flash->error(__('Alguien más está editando el artículo.'));
            return $this->redirect(['controller'=>'venta','action'=>'index', $idUsuario]);
        }

        //Se modifica el estado a bloqueado
        if ($this->request->is(['get'])) {
            //Se obtiene la tabla para hacer la transacción manualmente
            $dataSource = $this->getTableLocator()->get('articulo');
            $articulo->bloqueo = true;

            //Se guarda la información modificada haciendo un commit
            try {
                $dataSource->getConnection()->begin();
                $dataSource->saveOrFail($articulo, ['atomic' => false]);
                $dataSource->getConnection()->commit();
                $this->Flash->success(__('Se está editando el artículo.'));
            //Se hace un rollback en dado caso de no haberse guardado la información de forma correcta
            } catch(\Cake\ORM\Exception\PersistenceFailedException $e) {
                $dataSource->getConnection()->rollback();
                $this->Flash->error(__('Error al momento de guardar la información.'));
            }
        }

        $this->set(compact('usuario'));
        $this->set(compact('articulo'));
    }
}
