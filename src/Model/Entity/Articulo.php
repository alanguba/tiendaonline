<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Articulo Entity
 *
 * @property int $idArticulo
 * @property string $nombre
 * @property string $descripcion
 * @property string $proveedor
 * @property float $precio
 * @property int $calificacion
 * @property int|null $idUsuario
 */
class Articulo extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'nombre' => true,
        'descripcion' => true,
        'proveedor' => true,
        'precio' => true,
        'calificacion' => true,
        'bloqueo' => true,
        'idUsuario' => true,
    ];
}
