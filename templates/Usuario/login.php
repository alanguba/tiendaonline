<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<link rel="stylesheet" type="text/css" href="/tiendaonline/css/styles.css">


<div class="main">
    <?= $this->Flash->render() ?>
    <div class="sidenav">
        <div class="login-main-text">
            <h2>Armazon</h2>
            <img class="img-fluid" src="/tiendaonline/img/armazon.png" alt="">
        </div>
    </div>
    <div class="col-md-6 col-sm-12">
        <div class="login-form">
            <?=$this->Form->create();?>
                <div class="form-group">
                    <label>Correo electrónico</label>
                    <?= $this->Form->input('email', ['type' => 'email', 'class' => 'form-control', 'placeholder' => 'Correo electrónico']);?> 
                </div>
                <div class="form-group">
                    <label>Contraseña</label>
                    <?= $this->Form->input('password', ['type' => 'password', 'class' => 'form-control', 'placeholder' => 'Contraseña']);?> 
                </div>
                <button type="submit" class="btn btn-black">Iniciar sesión</button>
            <?= $this->Form->end();?>
        </div>
    </div>
</div>