<div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
  <h1 class="display-4">Editar información producto</h1>
  <img style="height: 300px;" src=<?php echo  "/tiendaonline/img/$articulo->idArticulo.jpg" ?> alt="">
</div>
<div class="text-center">
    <span class="badge bg-primary">Libros</span>
    <span class="badge bg-secondary">Videojuegos</span>
    <span class="badge bg-success">Ropa</span>
    <span class="badge bg-danger">Electrónica</span>
    <span class="badge bg-warning text-dark">Software</span>
    <span class="badge bg-info text-dark">Electrodomésticos</span>
    <span class="badge bg-light text-dark">Alimetos</span>
    <span class="badge bg-dark">Juguetes</span>
</div>
<div class="container">
    <?=$this->Form->create($articulo);?>
        <div class="form-row">
            <div class="form-group col-md-12">
                <label for="nombre">Nombre</label>
                <?= $this->Form->input('nombre', ['type' => 'text', 'class' => 'form-control']);?> 
            </div>
            <div class="form-group col-md-12">
                <label for="descripcion">Descripción</label>
                <?= $this->Form->control('descripcion', ['rows' => '3', 'class' => 'form-control']); ?> 
            </div>
            <div class="form-group col-md-12">
                <label for="proveedor">Proveedor</label>
                <?= $this->Form->input('proveedor', ['type' => 'text', 'class' => 'form-control']);?> 
            </div>
            <div class="form-group col-md-12">
                <label for="precio">Precio</label>
                <?= $this->Form->input('precio', ['type' => 'number', 'class' => 'form-control', 'min' => '0']);?> 
            </div>
            <div class="form-group col-md-12">
                <label for="precio">Calificación</label>
                <?= $this->Form->input('calificacion', ['type' => 'number', 'class' => 'form-control', 'min' => '0', 'max' => '5']);?> 
            </div>
        </div>
        <br>
        <button type="submit" class="btn btn-primary">Editar</button>
    <?= $this->Form->end();?>
</div>