<main>

  <section class="py-5 text-center container">
    <div class="row py-lg-5">
      <div class="col-lg-6 col-md-8 mx-auto">
        <h1 class="fw-light">Venta de productos</h1>
        <img class="img-fluid" src="/tiendaonline/img/armazon.png" alt="">
      </div>
    </div>
  </section>

    <div class="album py-5 bg-light">
      <div class="container">
        <div class="row">
        <!-- Se hace una interación para mostrar todos los artículos -->
        <?php foreach ($articulos as $articulo): ?>

          <div class="col-md-4">
              <div class="card mb-4 box-shadow">
                <img style="height: 300px;" src=<?php echo  "/tiendaonline/img/$articulo->idArticulo.jpg" ?> alt="">
                <ul class="list-unstyled mt-3 mb-4">
                  <?php for ($i = 0; $i < $articulo->calificacion; $i++):?>
                      <i class="fas fa-star"></i>
                  <?php endfor; ?>
                  <li> <strong>Artículo:</strong> <?=$articulo->nombre?></li>
                  <li><strong>Descripción</strong></li>
                  <div style="text-align: justify; text-justify: inter-word;"><?=$articulo->descripcion?></div>
                </ul>
                <div class="card-body">
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <!-- Se pregunta si el artículo ya fue comprado para mostrar el botón de compra o no -->
                      <?php if ($articulo->idUsuario == 0) : ?>
                        <a href=<?php echo "/tiendaonline/venta/comprar/$articulo->idArticulo/$usuario->idUsuario" ?> class="btn btn-sm btn-success" role="button" aria-pressed="true">Comprar</a>
                      <?php else:  ?>
                        <button type="button" class="btn btn-sm btn-danger" disabled>Agotado</button>
                      <?php endif;  ?>
                      <!-- Se pregunta si es un usuario administrador, para mostrar el botón de edición o no -->
                      <?php if ($usuario->rol == false) : ?>
                        <a href=<?php echo "/tiendaonline/venta/editar/$articulo->idArticulo/$usuario->idUsuario" ?> class="btn btn-sm btn-warning" role="button" aria-pressed="true">Editar</a>
                      <?php endif;  ?>
                      
                    </div>
                  
                    <small class="text-muted"><?php echo $articulo->proveedor ?></small>
                    <small class="text-primary"><?php echo "$".$articulo->precio ?></small>
                  </div>
                </div>
              </div>
            </div>

         
        <?php endforeach; ?>

      </div>
    </div>
  </div>

</main>