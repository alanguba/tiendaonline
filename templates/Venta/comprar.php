<?php
 $total = $usuario->credito - $articulo->precio;
?>
<div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
  <h1 class="display-4">Compra ahora</h1>
  <p class="lead">La moneda utilizada en la tienda es a través de cupones, no se utilizan transacciones directas con ningún banco.</p>
</div>
<div class="text-center">
    <span class="badge bg-primary">Libros</span>
    <span class="badge bg-secondary">Videojuegos</span>
    <span class="badge bg-success">Ropa</span>
    <span class="badge bg-danger">Electrónica</span>
    <span class="badge bg-warning text-dark">Software</span>
    <span class="badge bg-info text-dark">Electrodomésticos</span>
    <span class="badge bg-light text-dark">Alimetos</span>
    <span class="badge bg-dark">Juguetes</span>
</div>
<br>
<?php if ($articulo->idUsuario): ?>
        <div class="alert alert-danger" role="alert">
            Ya no hay disponibilidad en el inventario—intenta más tarde.
        </div>
    <?php endif;?>
<div class="container d-flex justify-content-center">
    <div class="row">
        <div class="card <?= $articulo->idUsuario ? 'border-danger' : 'border-success'?>" style="width: 18rem;">
            <div class="card-header">
                <h4 class="my-0 font-weight-normal <?= $articulo->idUsuario ? 'text-danger' : 'text-success'?>"><?=$articulo->nombre?></h4>
            </div>
            <div class="card-body">
            <?=$this->Form->create($usuario);?>
                <h1 class="card-title pricing-card-title"><?=$articulo->precio?> <small class="text-muted"> points</small></h1>
                <ul class="list-unstyled mt-3 mb-4">
                <?php for ($i = 0; $i < $articulo->calificacion; $i++):?>
                    <i class="fas fa-star"></i>
                <?php endfor; ?>
                <li> <strong>Proveedor:</strong> <?=$articulo->proveedor?></li>
                <li><strong>Descripción</strong></li>
                <div style="text-align: justify; text-justify: inter-word;"><?=$articulo->descripcion?></div>
                </ul>
               <?= $this->Form->input('credito', ['value' => $total, 'type' => 'hidden']);?>
                <?php if ($articulo->idUsuario): ?>
                    <button type="submit" class="btn btn-lg btn-block btn-outline-danger" disabled>Confirmar</button>
                <?php else:?>
                    <button type="submit" class="btn btn-lg btn-block btn-outline-success">Confirmar</button>
                <?php endif;?>
                <?= $this->Form->end();?>
            </div>
        </div>
        <div class="p-4 mb-3 bg-light rounded col">
            <h4 class="font-italic">Al realizar la compra</h4>
            <p class="mb-0">&nbsp&nbsp&nbsp&nbsp<strong><?=$usuario->credito?></strong></p>
            <i class="fas fa-minus"></i>
            <p class="mb-0 text-danger">&nbsp&nbsp&nbsp&nbsp<strong><?=$articulo->precio?></strong></p>
            <i class="fas fa-equals"></i>
            <?php if ($total < 0): ?>
                <p class="text-danger">&nbsp&nbsp&nbsp<strong><?=$total?></strong></p>
            <?php else:?>
                <p class="text-primary">&nbsp&nbsp&nbsp<strong><?=$total?></strong></p>
             <?php endif;?>
        </div>
    </div>
</div>