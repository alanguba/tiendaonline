<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">

    <?= $this->Html->css("bootstrap.min.css") ?>
    <?= $this->Html->css("all.min.css") ?>
    <?= $this->Html->script("bootstrap.min.js") ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

</head>
<body>
  <!-- No se muestra en dado caso de ser página de login -->
  <?php if (!isset($login)): ?>
    <!-- Nav -->
    <div class="navbar d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
        <h5 class="my-0 mr-md-auto font-weight-normal">Armazon</h5>
        <nav class="my-2 my-md-0 mr-md-3">
        <h5 class="my-0 mr-md-auto font-weight-normal"><?php echo $usuario->nombre?></h5>
          <h5 class="my-0 mr-md-auto font-weight-normal"><?php echo "Crédito restante: $".$usuario->credito?></h5>
        </nav>
         <!-- Se redirige a la pestaña de login -->
        <a class="btn btn-outline-danger" href="/tiendaonline/usuario/login">Cerrar sesión</a>
    </div>
  <?php endif; ?>
    <!-- Contenido principal -->
        <?= $this->Flash->render() ?>
        <?= $this->fetch('content') ?>
    <!-- No se muestra en dado caso de ser página de login -->
    <?php if (!isset($login)): ?>
    <footer class="pt-4 my-md-5 border-top">
        <div class="row" style="padding-left: 50px;">
            <div class="col-12 col-md">
            <small class="d-block mb-3 text-muted">© 2021</small>
        </div>
      <div class="col-5 col-md">
        <h5>Conócenos</h5>
        <ul class="list-unstyled text-small">
          <li><a class="text-muted" href="#">Trabaja con nosotros</a></li>
          <li><a class="text-muted" href="#">Información corporativa</a></li>
          <li><a class="text-muted" href="#">Departamento de prensa</a></li>
        </ul>
      </div>
      <div class="col-5 col-md">
        <h5>Acerca de la tienda</h5>
        <ul class="list-unstyled text-small">
          <li><a class="text-muted" href="#">Cómo comprar</a></li>
          <li><a class="text-muted" href="#">Seguimiento de pedido</a></li>
          <li><a class="text-muted" href="#">Realizar devoluciones</a></li>
          <li><a class="text-muted" href="#">Quejas y sugerencias</a></li>
        </ul>
      </div>
    </div>
    </footer>
  <?php endif; ?>
</body>
</html>
