CREATE TABLE usuario (
	idUsuario INT NOT NULL AUTO_INCREMENT,
	nombre VARCHAR(50) NOT NULL,
	apPat VARCHAR(30) NOT NULL,
	apMat VARCHAR(30) NOT NULL,
	email VARCHAR(50) NOT NULL,
	credito FLOAT NOT NULL,
	password VARCHAR(30) NOT NULL,
	rol BOOLEAN NOT NULL,
	PRIMARY KEY (idUsuario)
);

CREATE TABLE articulo (
	idArticulo INT NOT NULL AUTO_INCREMENT,
	nombre VARCHAR(50) NOT NULL,
	descripcion TEXT NOT NULL,
	proveedor VARCHAR(50) NOT NULL,
	precio FLOAT NOT NULL,
	calificacion INT NOT NULL,
	bloqueo BOOLEAN NOT NULL DEFAULT false,
	idUsuario INT NULL,
	
	PRIMARY KEY (idArticulo),
	FOREIGN KEY (idUsuario) REFERENCES usuario(idUsuario)
);

--USUARIOS
INSERT INTO usuario (nombre,apPat, apMat, email, credito, password, rol) 
VALUES ('Alan','Gutierrez', 'Banuelos', 'alan@sd.com', 20000.00, 'Alan', TRUE);

INSERT INTO usuario (nombre,apPat, apMat, email, credito, password, rol) 
VALUES ('Ulises','Herrera', 'Martinez', 'ulises@sd.com', 20000.00, 'Ulises', TRUE);

INSERT INTO usuario (nombre,apPat, apMat, email, credito, password, rol) 
VALUES ('Andres','Martinez', 'Martinez', 'andres@sd.com', 20000.00, 'Andres', TRUE);

INSERT INTO usuario (nombre,apPat, apMat, email, credito, password, rol) 
VALUES ('Uzziel','Palma', 'Rodriguez', 'uzziel@sd.com', 20000.00, 'Uzziel', TRUE);

--Usuario administrador
INSERT INTO usuario (nombre,apPat, apMat, email, credito, password, rol) 
VALUES ('Luis','Perez', 'Hernandez', 'luis@gmail.com', 800.00, 'Luis', FALSE);

INSERT INTO usuario (nombre,apPat, apMat, email, credito, password, rol) 
VALUES ('Miriam','Reyes', 'Cruz', 'miriamreyes@gmail.com', 5000.00, 'Miriam', TRUE);

INSERT INTO usuario (nombre,apPat, apMat, email, credito, password, rol) 
VALUES ('Diana','Alcantara', 'Gomez', 'dianacantara@gmail.com', 10000.00, 'Diana', TRUE);

INSERT INTO usuario (nombre,apPat, apMat, email, credito, password, rol) 
VALUES ('Abigail','Godoy', 'Vidaure', 'abigovi97@gmail.com', 14000.00, 'Abigail', TRUE);

INSERT INTO usuario (nombre,apPat, apMat, email, credito, password, rol) 
VALUES ('Gisel','Bazaldua', 'Lopez', 'karengisel00@gmail.com', 12000.00, 'Karen Gisel', TRUE);

INSERT INTO usuario (nombre,apPat, apMat, email, credito, password, rol) 
VALUES ('Paulina','Garcia', 'Racilla', 'paulinagarcia00@gmail.com', 12000.00, 'Paulina', TRUE);


--ARTICULOS
INSERT INTO articulo (nombre,descripcion, proveedor, precio, calificacion) 
VALUES ('PS5','Juegos asombrosos: Maravíllate con asombrosos gráficos y descubre las nuevas funciones de PS5.
Velocidad sorprendente: Disfruta de la potencia de una CPU, una GPU y una SSD personalizadas con E/S integradas que redefinirán lo que una consola PlayStation puede hacer.
', 'Sony', 10000.0, 5);

INSERT INTO articulo (nombre,descripcion, proveedor, precio, calificacion) 
VALUES ('iPhone 12','Sistema avanzado de cámara dual de 12 MP con gran angular y ultra gran angular, modo Noche, Deep Fusion, HDR Inteligente 3 y grabación de vídeo en 4K HDR con Dolby Vision.
Compatibilidad con accesorios MagSafe, que se acoplan fácilmente y permiten una carga inalámbrica más rápida', 'Apple', 20000.0, 5);

INSERT INTO articulo (nombre,descripcion, proveedor, precio, calificacion) 
VALUES ('Persil Universal 5kg','Elimina hasta las manchas más difíciles dejando resultados perfectos.
Deja una larga y placentera sensación de frescura en tu ropa.
Les da un brillo único a tus prendas, incluso después de varias lavadas.
Conoce el poder máximo de Persil con tecnología alemana', 'Amazon', 1000.0, 3);

INSERT INTO articulo (nombre,descripcion, proveedor, precio, calificacion) 
VALUES ('Funko Iron Spider','Funko POP de Avengers:Endgame. Dimensiones: 6.35 x 6.35 x 9.53 cm; 150 g', 'Funko', 800.0, 4);


INSERT INTO articulo (nombre,descripcion, proveedor, precio, calificacion) 
VALUES ('Assassins Creed','Videojuego para PS5', 'Gamestop', 2000.0, 3);

INSERT INTO articulo (nombre,descripcion, proveedor, precio, calificacion) 
VALUES ('El principito','El Principito es uno de los clásicos más importantes de la literatura, 
	con más de 134 millones de ejemplares vendidos desde su primera edición en 1943 y traducido a 220 diferentes idiomas y dialectos. ', 'Amazon', 150.0, 4);


INSERT INTO articulo (nombre,descripcion, proveedor, precio, calificacion) 
VALUES ('Oreo','Caja con 21 paquetes de deliciosas galletas oreo.
Acompáñalo con leche, o cualquier bebida que desees.
Incluyelo en tus postres.
Ideal para toda la familia.
Llévalo en tú lunch.', 'Walmart', 350.0, 5);


INSERT INTO articulo (nombre,descripcion, proveedor, precio, calificacion) 
VALUES ('Video de Alta Definición', 'La Tapo C200 captura cada detalle con alta definicion ( Full HD1080P).
Visión Nocturna. Sepa qué sucede cuando está oscuro, mediante nuestro avanzado sistema infrarojo controle un rango de hasta 9 mts.', 'Amazon', 670.0, 4);


INSERT INTO articulo (nombre,descripcion, proveedor, precio, calificacion) 
VALUES ('Engrapadora de oficina','La grapadora puede contener hasta 200 grapas y grapa hasta 10 hojas
Se puede abrir para pegar información a un tablón de anuncios; invierte el yunque para fijar documentos.
La base de goma completa mantiene la grapadora firmemente en su lugar durante el uso', 'Amazon', 100.0, 4);



INSERT INTO articulo (nombre,descripcion, proveedor, precio, calificacion) 
VALUES ('Mouse','Sensor de alta precisión con DPI ajustable hasta 8000 DPI
Iluminación RGB LIGHTSYNC. LIGHTSYNC RGB personalizable con efecto de onda de color personalizable con aproximadamente 16,8 millones de colores
Diseño de 6 botones probado con el tiempo con botones programables usando Software Logitech G HUB', 'Amazon', 470.0, 2);

INSERT INTO articulo (nombre,descripcion, proveedor, precio, calificacion) 
VALUES('Sudadera de hombre con capucha', 
'Sudadera de diseño a bloques en color Vino , Avena y Marino , con jareta en capucha y bolsa de canguro. Poliester', 
'Shein', 1290.00, 4);

INSERT INTO articulo (nombre,descripcion, proveedor, precio, calificacion) 
VALUES('Sombrero de hombre', 
'Sombrero con diseño de cinturón de cadena. Liso y 100% Poliester. Casual color negro', 
'Shein', 999.00, 2);


INSERT INTO articulo (nombre,descripcion, proveedor, precio, calificacion) 
VALUES('Audifonos para gaming', 
'Almohadillas de memory foam, Micrófono con cancelación de ruido, compatible con PC, Xbox One, PS4, Nintendo Switch y 
dispositivos móviles HX-HSCS-BK/NA', 
'Liverpool', 3099.00, 2);


INSERT INTO articulo (nombre,descripcion, proveedor, precio, calificacion) 
VALUES('Cafetera', 
'Cafetera programable de 12 tazas, plateado y negro', 
'Liverpool', 2499.99, 4);

INSERT INTO articulo (nombre,descripcion, proveedor, precio, calificacion) 
VALUES('Lámpara de Mesa', 
'Estilo moderno. Luminaria de escritorio. 1 cabeza con pantalla de metal Negro/Blanco/Rosa en forma de tambor para dormitorio - 110V-120V Rosa', 
'Litfad', 2105.60, 4);


INSERT INTO articulo (nombre,descripcion, proveedor, precio, calificacion) 
VALUES('Gafas con filtro luz azul', 
'Son ligeras, elegantes y tiene el tamaño perfecto para la graduación que se requiere. Recomendable si estas en busca de algo que pueda combinarse con tu guardarropa casual', 
'Amazon', 1599.99, 5);

INSERT INTO articulo (nombre,descripcion, proveedor, precio, calificacion) 
VALUES('Originals Humphrey Elefante de Peluche', 
'El elefante de peluche Choo Choo Express mide aproximadamente 100 cm de largo x 80 cm de ancho, cuando está sentado. Está fabricado 100% poliéster. 
Instrucciones de cuidado: lavar a máquina en ciclo suave, secar en ciclo lento y quitar rápidamente', 'Amazon', 1253.00, 3);

INSERT INTO articulo (nombre,descripcion, proveedor, precio, calificacion) 
VALUES('Tenis mujer Adidas', 'Los tenis Adidas Grand Court con líneas simples le imprimen un look limpio y fresco.
Cuenta con detalles a color en la parte del talón y lengüeta, combinando con las franjas laterales. Talla 24 cm', 'Adidas', 1599.00, 4);

INSERT INTO articulo (nombre,descripcion, proveedor, precio, calificacion) 
VALUES('Playera de Manga Larga para Mujer', 
'60% poliéster, 35% algodón, 5% licra. Cierre de elástico. Talla mediana. Camiseta de manga larga para mujer, suelta, con tiras, 
hombros descubiertos, informal, para mujer. Bonita blusa perfecta para la primavera, otoño e invierno', 'Ezbelle', 1025.91, 5);

INSERT INTO articulo (nombre,descripcion, proveedor, precio, calificacion) 
VALUES('Kit de maquillaje', 
'El set de maquillaje primario de alta calidad está completamente equipado, es fácil de transportar, seguro y confiable. Efecto de color duradero, 
textura suave y confortable; equipado con un cepillo de alta calidad, fácil de crear un maquillaje perfecto. Contiene sombra de ojos multicolor mate 
y con brillo, brillo de labios, rímel, crema para ojos, lápiz de maquillaje, corrector, etc., para satisfacer sus necesidades diarias de maquillaje', 
'Ezbelle', 2025.91, 4);
